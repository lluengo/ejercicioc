package ar.com.lluengo.ejercicioc.views.bank;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import ar.com.lluengo.ejercicioc.R;
import ar.com.lluengo.ejercicioc.entities.bank.CardIssuer;
import ar.com.lluengo.ejercicioc.entities.payment_method.PaymentMethod;
import ar.com.lluengo.ejercicioc.service.ApiUtils;
import ar.com.lluengo.ejercicioc.service.SoService;
import ar.com.lluengo.ejercicioc.utilities.Parameter;
import ar.com.lluengo.ejercicioc.utilities.Util;
import ar.com.lluengo.ejercicioc.views.installments.InstallmentView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardIssuerView extends AppCompatActivity {

    private CardIssuerAdapter adapter;
    private ListView listView;

    private SoService mService;

    private List<CardIssuer> cardIssuers;

    private int mount;
    private String payment_id;

    // -------------------------------------------------------------------------

    @Override
    protected void onCreate( Bundle savedInstanceState ) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        listView = (ListView) findViewById(R.id.universal_list);

        payment_id  = getIntent().getStringExtra("Id");
        mount       = getIntent().getIntExtra("Mount",0);

        adapter = new CardIssuerAdapter(this , getApplicationContext());
        listView.setOnItemClickListener( actionContinue );

        listView.invalidateViews();
        listView.setAdapter( adapter );

        mService = ApiUtils.getSOService();
        mService = ApiUtils.getSOService();

        loadAnswers();
    }

    // -------------------------------------------------------------------------

    public void loadAnswers() {

        Parameter parameter = new Parameter();
        parameter.addParameter("public_key"         , Util.key);
        parameter.addParameter("payment_method_id"  , payment_id);

        mService.getCardIssuers(parameter.getParameters()).enqueue(new Callback<List<CardIssuer>>() {
            @Override
            public void onResponse(Call<List<CardIssuer>> call, Response<List<CardIssuer>> response) {
                if(response.isSuccessful()) {
                    try {
                        cardIssuers = response.body();
                        buildView(cardIssuers);
                    }
                    catch (Exception e){
                        Log.e("responseGetCardIssuers", e.getMessage());
                    }

                }else {
                    Log.e("responseGetCardIssuers", "Error al procesar la solicitud");
                }

            }

            @Override
            public void onFailure(Call<List<CardIssuer>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"No se pudo obtener los datos",Toast.LENGTH_SHORT).show();
                Log.e("getCardIssuers", t.getMessage());
            }

        });
    }

    private void buildView( List<CardIssuer> cardIssuers )
    {
        if (cardIssuers.size() == 0){
            dispatchIntentContinue(payment_id,mount,"-");
            finish();
            return;
        }
        adapter.setItems( cardIssuers );
        adapter.notifyDataSetChanged();
    }

    // -------------------------------------------------------------------------

    private final AdapterView.OnItemClickListener actionContinue = new AdapterView.OnItemClickListener(){
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id )
        {
            if (cardIssuers == null) return;
            CardIssuer ci = cardIssuers.get(position);
            dispatchIntentContinue(payment_id,mount,ci.getId());
        }
    };

    // -------------------------------------------------------------------------

    public void dispatchIntentContinue(String id, int mount,String issuerId)
    {

        Intent i = new Intent( this, InstallmentView.class );
        i.putExtra( "Id"    , id );
        i.putExtra( "Mount" , mount );
        i.putExtra("IssuerId",issuerId);
        startActivity( i );

    }

}
