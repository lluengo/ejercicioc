package ar.com.lluengo.ejercicioc.views.installments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import ar.com.lluengo.ejercicioc.App;
import ar.com.lluengo.ejercicioc.R;
import ar.com.lluengo.ejercicioc.entities.Installments.Installment;
import ar.com.lluengo.ejercicioc.entities.Installments.PayerCost;
import ar.com.lluengo.ejercicioc.service.ApiUtils;
import ar.com.lluengo.ejercicioc.service.SoService;
import ar.com.lluengo.ejercicioc.utilities.Parameter;
import ar.com.lluengo.ejercicioc.utilities.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InstallmentView extends AppCompatActivity {

    private InstallmentAdapter adapter;
    private ListView listView;

    private SoService mService;

    private int mount;
    private String payment_id;
    private String issuer_id;

    private String issuer;
    private String payment;

    private List<PayerCost> payerCosts;

    // -------------------------------------------------------------------------

    @Override
    protected void onCreate( Bundle savedInstanceState ) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        listView = (ListView) findViewById(R.id.universal_list);

        payment_id  = getIntent().getStringExtra("Id");
        mount       = getIntent().getIntExtra("Mount",0);
        issuer_id   = getIntent().getStringExtra("IssuerId");

        adapter = new InstallmentAdapter(this , getApplicationContext());
        listView.setOnItemClickListener( actionContinue );

        listView.invalidateViews();
        listView.setAdapter( adapter );

        mService = ApiUtils.getSOService();
        mService = ApiUtils.getSOService();

        loadAnswers();
    }

    // -------------------------------------------------------------------------

    public void loadAnswers() {

        Parameter parameter = new Parameter();
        parameter.addParameter("public_key"         , Util.key);
        parameter.addParameter("payment_method_id"  , payment_id);

        if (!issuer_id.equals("-"))
            parameter.addParameter("issuer.id", issuer_id);

        mService.getInstallments(parameter.getParameters()).enqueue(new Callback<List<Installment>>() {
            @Override
            public void onResponse(Call<List<Installment>> call, Response<List<Installment>> response) {
                if(response.isSuccessful()) {
                    try {
                        buildView(response.body());
                    }
                    catch (Exception e){
                        Log.e("responseGetCardIssuers", e.getMessage());
                    }

                }else {
                    Log.e("responseGetCardIssuers", "Error al procesar la solicitud");
                }
            }

            @Override
            public void onFailure(Call<List<Installment>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"No se pudo obtener los datos",Toast.LENGTH_SHORT).show();
                Log.e("getCardIssuers", t.getMessage());
            }

        });
    }

    // -------------------------------------------------------------------------

    private void buildView( List<Installment> installments )
    {
        payerCosts = new ArrayList<>();
        for (Installment cd : installments){
            issuer = cd.getIssuer() != null ? cd.getIssuer().getName() : "";
            payment = cd.getPaymentMethodId();
            for (PayerCost pc : cd.getPayerCosts()) payerCosts.add(pc);

        }
        adapter.setItems( payerCosts );
        adapter.notifyDataSetChanged();
    }

    // -------------------------------------------------------------------------

    private final AdapterView.OnItemClickListener actionContinue =
            new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id )
                {
                    if (payerCosts == null) return;
                    PayerCost pc = payerCosts.get(position);

                    String message = "Usted seleccionó el siguiente pago: \n" +
                            "Importe: " + mount + "\n" +
                            "Medio de pago: " + payment + "\n" +
                            "Banco: " + issuer + "\n" +
                            "Cuotas: " + pc.getRecommendedMessage();

                    Intent i = new Intent( getApplicationContext(), App.class );
                    i.putExtra("allow"      ,true);
                    i.putExtra("message"    , message );
                    startActivity( i );
                    finish();
                }
            };

}
