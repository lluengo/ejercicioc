package ar.com.lluengo.ejercicioc.views.payment_method;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import java.util.List;
import ar.com.lluengo.ejercicioc.R;
import ar.com.lluengo.ejercicioc.entities.payment_method.PaymentMethod;
import ar.com.lluengo.ejercicioc.service.ApiUtils;
import ar.com.lluengo.ejercicioc.service.SoService;
import ar.com.lluengo.ejercicioc.utilities.Parameter;
import ar.com.lluengo.ejercicioc.utilities.Util;
import ar.com.lluengo.ejercicioc.views.bank.CardIssuerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentMethodView extends AppCompatActivity {

    private PaymentMethodAdapter			adapter;
    private ListView listView;
    private SoService mService;
    private List<PaymentMethod> paymentMethods;

    private int mount;

    // -------------------------------------------------------------------------

    @Override
    protected void onCreate( Bundle savedInstanceState ) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        listView = (ListView) findViewById(R.id.universal_list);

        mount = getIntent().getIntExtra("Mount",0);

        adapter = new PaymentMethodAdapter(this , getApplicationContext());
        listView.setOnItemClickListener( actionContinue );

        listView.invalidateViews();
        listView.setAdapter( adapter );

        mService = ApiUtils.getSOService();

        loadAnswers();
    }

    // -------------------------------------------------------------------------

    public void loadAnswers() {

        Parameter parameter = new Parameter();
        parameter.addParameter("public_key", Util.key);

        mService.getPaymentMethod(parameter.getParameters()).enqueue(new Callback<List<PaymentMethod>>() {
            @Override
            public void onResponse(Call<List<PaymentMethod>> call, Response<List<PaymentMethod>> response) {
                if(response.isSuccessful()) {
                    try {
                        paymentMethods = response.body();
                        buildView(paymentMethods);
                    }
                    catch (Exception e){
                        Log.e("responsePaymentMethod", e.getMessage());
                    }

                }else {
                    Log.e("responsePaymentMethod", "Error al procesar la solicitud");
                }

            }

            @Override
            public void onFailure(Call<List<PaymentMethod>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"No se pudo obtener los datos",Toast.LENGTH_SHORT).show();
                Log.e("getCardIssuers", t.getMessage());
            }

        });
    }

    // -------------------------------------------------------------------------

    private void buildView( List<PaymentMethod> paymentMethods )
    {
        adapter.setItems( paymentMethods );
        adapter.notifyDataSetChanged();
    }

    // -------------------------------------------------------------------------

    private final AdapterView.OnItemClickListener actionContinue = new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id )
                {
                    if (paymentMethods == null) return;
                    PaymentMethod pm = paymentMethods.get(position);
                    dispatchIntentContinue(pm.getId(),mount);
                }
            };

    // -------------------------------------------------------------------------

    public void dispatchIntentContinue(String id, int mount)
    {
        Intent i = new Intent( this, CardIssuerView.class );
        i.putExtra( "Id"    , id );
        i.putExtra( "Mount" , mount );
        startActivity( i );


    }

}
