package ar.com.lluengo.ejercicioc.views.installments;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import ar.com.lluengo.ejercicioc.R;
import ar.com.lluengo.ejercicioc.entities.Installments.PayerCost;

public class InstallmentAdapter extends BaseAdapter
{

    private final List<PayerCost> mItems;

    private final LayoutInflater inflater;
    Context context;
    Activity mainActivity;

    // -------------------------------------------------------------------------

    public void setItems( List<PayerCost> data )
    {
        mItems.clear();
        mItems.addAll( data );
    }

    //------------------------------------------------------------------------------------------------------------------

    public InstallmentAdapter(Activity a, Context context) {

        mItems 		= new ArrayList<PayerCost>();
        inflater 	= (LayoutInflater) a.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        this.context        = context;
        this.mainActivity   = a;

    }

    //------------------------------------------------------------------------------------------------------------------

    @Override
    public int getCount () {
        return mItems.size();
    }

    @Override
    public Object getItem( int position ) {
        return mItems.get( position );
    }

    //------------------------------------------------------------------------------------------------------------------

    @Override
    public long getItemId( int position ) {
        return position;
    }

    //------------------------------------------------------------------------------------------------------------------

    @Override
    public View getView(final int position, View convertView, ViewGroup parent )
    {
        View view = convertView != null ? convertView : inflater.inflate(R.layout.activity_item_list_row, null );

        TextView    leyenda     = (TextView) view.findViewById(R.id.description);
        TextView    type 		= (TextView)  view.findViewById( R.id.tipo );

        final PayerCost pc = mItems.get( position );

        leyenda.setText(pc.getRecommendedMessage());

        String label = "";
        for (String s : pc.getLabels()){ label += s + "\n"; }

        type.setText(label);

        return view;
    }

}