package ar.com.lluengo.ejercicioc.views.bank;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import ar.com.lluengo.ejercicioc.R;
import ar.com.lluengo.ejercicioc.entities.bank.CardIssuer;
import ar.com.lluengo.ejercicioc.utilities.ImageLoadTask;

public class CardIssuerAdapter extends BaseAdapter
{
    private final List<CardIssuer> mItems;

    private final LayoutInflater inflater;
    Context context;
    Activity mainActivity;

    // -------------------------------------------------------------------------

    public void setItems( List<CardIssuer> data )
    {
        mItems.clear();
        mItems.addAll( data );
    }

    //------------------------------------------------------------------------------------------------------------------

    public CardIssuerAdapter(Activity a, Context context) {

        mItems 		= new ArrayList<CardIssuer>();
        inflater 	= (LayoutInflater) a.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        this.context        = context;
        this.mainActivity   = a;

    }

    //------------------------------------------------------------------------------------------------------------------

    @Override
    public int getCount () {
        return mItems.size();
    }

    @Override
    public Object getItem( int position ) {
        return mItems.get( position );
    }

    //------------------------------------------------------------------------------------------------------------------

    @Override
    public long getItemId( int position ) {
        return position;
    }

    //------------------------------------------------------------------------------------------------------------------

    @Override
    public View getView(final int position, View convertView, ViewGroup parent )
    {
        View view = convertView != null ? convertView : inflater.inflate(R.layout.activity_item_list_row, null );

        TextView name 			= (TextView)  view.findViewById( R.id.description );
        ImageView imageView     = (ImageView) view.findViewById(R.id.image_bank);

        final CardIssuer ci = mItems.get( position );

        name.setText(ci.getName());

        imageView.setImageResource(android.R.color.transparent);
        new ImageLoadTask(ci.getThumbnail(),imageView).execute();

        return view;
    }
}