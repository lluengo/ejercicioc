package ar.com.lluengo.ejercicioc.service;

public class ApiUtils {

    public static final String BASE_URL = "https://api.mercadopago.com/v1/";

    public static SoService getSOService() {
        return RetrofitClient.getClient(BASE_URL).create(SoService.class);
    }
}
