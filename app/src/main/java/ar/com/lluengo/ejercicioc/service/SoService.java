package ar.com.lluengo.ejercicioc.service;

import java.util.List;
import java.util.Map;

import ar.com.lluengo.ejercicioc.entities.Installments.Installment;
import ar.com.lluengo.ejercicioc.entities.bank.CardIssuer;
import ar.com.lluengo.ejercicioc.entities.payment_method.PaymentMethod;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;


public interface SoService {

    @GET("payment_methods")
    Call<List<PaymentMethod>> getPaymentMethod(@QueryMap Map<String, String> params);

    @GET("payment_methods/card_issuers")
    Call<List<CardIssuer>> getCardIssuers(@QueryMap Map<String, String> params);

    @GET("payment_methods/installments")
    Call<List<Installment>> getInstallments(@QueryMap Map<String, String> params);


}