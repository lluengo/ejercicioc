package ar.com.lluengo.ejercicioc.utilities;


import java.util.HashMap;

public class Parameter {

    private HashMap<String,String> parameters;

    public Parameter (){
        parameters = new HashMap<>();
    }

    public HashMap<String, String> getParameters() {
        return parameters;
    }

    public void addParameter(String key, String value) {
        parameters.put(key,value);
    }


}
