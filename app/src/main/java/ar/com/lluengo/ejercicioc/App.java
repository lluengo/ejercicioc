package ar.com.lluengo.ejercicioc;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import ar.com.lluengo.ejercicioc.entities.payment_method.PaymentMethod;
import ar.com.lluengo.ejercicioc.service.ApiUtils;
import ar.com.lluengo.ejercicioc.service.SoService;
import ar.com.lluengo.ejercicioc.utilities.Parameter;
import ar.com.lluengo.ejercicioc.utilities.Util;
import ar.com.lluengo.ejercicioc.views.installments.InstallmentView;
import ar.com.lluengo.ejercicioc.views.payment_method.PaymentMethodView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class App extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount);

        Button nextButon = ( Button )   findViewById( R.id.bContinuar );

        nextButon.setOnClickListener(eContinue);

        if (getIntent().getBooleanExtra("allow",false)){

            AlertDialog alertDialog = new AlertDialog.Builder(App.this).create();
            alertDialog.setMessage(getIntent().getStringExtra("message"));
            alertDialog.show();
        }
    }

    // -------------------------------------------------------------------------

    View.OnClickListener eContinue = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            EditText etMonto = (EditText) findViewById(R.id.etMonto);
            int mount = 0;

            if (etMonto.getText() == null || etMonto.getText().length() > 8){
                Toast.makeText(getApplicationContext(),"Ingrese un valor válido",Toast.LENGTH_SHORT).show();
                return;
            }

            try {
                mount = Integer.parseInt(etMonto.getText().toString());
            }
            catch (NumberFormatException e) {
                Log.i("Convertion Error", "Convertion Error");
            }

            if (mount == 0 || mount > 9999999) {
                Toast.makeText(getApplicationContext(),"Ingrese un valor válido",Toast.LENGTH_SHORT).show();
                return;
            }

            Intent i = new Intent( getApplicationContext(), PaymentMethodView.class );
            i.putExtra( "Mount", mount );
            startActivity( i );
        }
    };

    // -------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        //Para que no pueda volver atras.
    }



}
