package ar.com.lluengo.ejercicioc;

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import ar.com.lluengo.ejercicioc.entities.Installments.Installment;
import ar.com.lluengo.ejercicioc.entities.payment_method.PaymentMethod;
import ar.com.lluengo.ejercicioc.service.ApiUtils;
import ar.com.lluengo.ejercicioc.service.SoService;
import ar.com.lluengo.ejercicioc.utilities.Parameter;
import ar.com.lluengo.ejercicioc.utilities.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.junit.Assert.*;

public class InstallmentUnitTest {

    private SoService mService;
    Parameter parameter = new Parameter();
    List<Installment> list;
    private final CountDownLatch latch = new CountDownLatch(1);

    @Before
    public void setUp() throws Exception {

        mService = ApiUtils.getSOService();
        parameter.addParameter("public_key", Util.key);
        parameter.addParameter("payment_method_id", "visa");
        parameter.addParameter("issuer.id", "288");
    }

    @After
    public void tearDown() throws Exception {
        list = null;
    }

    @Test
    public void getInstallments() throws Exception {

        mService.getInstallments(parameter.getParameters()).enqueue(new Callback<List<Installment>>() {
            @Override
            public void onResponse(Call<List<Installment>> call, Response<List<Installment>> response) {
                list = response.body();
                latch.countDown();
            }

            @Override
            public void onFailure(Call<List<Installment>> call, Throwable t) {
                latch.countDown();
            }


        });

        latch.await();
        Assert.assertNotNull(list);

    }
}