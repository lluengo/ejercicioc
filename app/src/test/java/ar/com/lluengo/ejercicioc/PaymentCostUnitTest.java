package ar.com.lluengo.ejercicioc;

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import ar.com.lluengo.ejercicioc.entities.payment_method.PaymentMethod;
import ar.com.lluengo.ejercicioc.service.ApiUtils;
import ar.com.lluengo.ejercicioc.service.SoService;
import ar.com.lluengo.ejercicioc.utilities.Parameter;
import ar.com.lluengo.ejercicioc.utilities.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.junit.Assert.*;

public class PaymentCostUnitTest {

    private SoService mService;
    Parameter parameter = new Parameter();
    List<PaymentMethod> list;
    private final CountDownLatch latch = new CountDownLatch(1);

    @Before
    public void setUp() throws Exception {

        mService = ApiUtils.getSOService();
        parameter.addParameter("public_key", Util.key);


    }

    @After
    public void tearDown() throws Exception {
        list = null;
    }

    @Test
    public void getPaymentCost() throws Exception {

        mService.getPaymentMethod(parameter.getParameters()).enqueue(new Callback<List<PaymentMethod>>() {
            @Override
            public void onResponse(Call<List<PaymentMethod>> call, Response<List<PaymentMethod>> response) {
                list = response.body();
                latch.countDown();
            }

            @Override
            public void onFailure(Call<List<PaymentMethod>> call, Throwable t) {
                latch.countDown();
            }


        });

        latch.await();
        Assert.assertNotNull(list);

    }
}